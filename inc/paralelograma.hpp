#ifndef PARALELOGRAMA_HPP
#define PARALELOGRAMA_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Paralelograma: public FormaGeometrica{



public:
Paralelograma();
Paralelograma(string tipo, float base, float altura);
~Paralelograma();
void imprime();
float calcula_perimetro();


};
#endif
