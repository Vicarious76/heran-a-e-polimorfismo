#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Circulo: public FormaGeometrica{
private:
  float raio;


public:
Circulo();
Circulo(string tipo, float base, float altura);
~Circulo();
void imprime();
void set_raio(float raio);
float get_raio();
float calcula_area();
float calcula_perimetro();


};
#endif
