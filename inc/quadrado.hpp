#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Quadrado: public FormaGeometrica{
  public:
Quadrado();
Quadrado(string tipo, float base, float altura);
~Quadrado();
void imprime();


};
#endif
