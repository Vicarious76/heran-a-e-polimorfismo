#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Hexagono: public FormaGeometrica {



public:
Hexagono();
Hexagono(string tipo, float base, float altura);
~Hexagono();
void imprime();
float calcula_area();
float calcula_perimetro();



};
#endif
