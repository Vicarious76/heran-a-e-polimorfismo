#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Triangulo: public FormaGeometrica {


  public:
Triangulo();
Triangulo(string tipo, float base, float altura);
~Triangulo();
void imprime();
float calcula_area();
float calcula_perimetro();



};

#endif
