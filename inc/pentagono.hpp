#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Pentagono: public FormaGeometrica{



public:
Pentagono();
Pentagono(string tipo, float base, float altura);
~Pentagono();
void imprime();
float calcula_area();
float calcula_perimetro();


};
#endif
