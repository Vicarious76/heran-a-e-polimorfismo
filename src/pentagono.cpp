#include "pentagono.hpp"
#include <iostream>

Pentagono::Pentagono(){
  cout << "Construtor da classe pentagono" << endl;
}

Pentagono::Pentagono(string tipo, float base, float altura){
  cout <<"construtor de sobrecarga da classe pentagono" << endl;
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}
Pentagono::~Pentagono(){
  cout << "destrutor da classe pentagono" << endl;
}
void Pentagono::imprime(){
  std::cout << "area:"<< calcula_area() << std::endl;
  std::cout << "perimetro:"<< calcula_perimetro() << std::endl;
}

float Pentagono::calcula_area(){
  return ((5 * get_base() * get_altura())/2);
}

float Pentagono::calcula_perimetro(){
  return (5 * get_altura());
}
