#include "quadrado.hpp"
#include <iostream>

Quadrado::Quadrado(){
  cout << "Construtor da classe quadrado" << endl;
}

Quadrado::Quadrado( string tipo, float base, float altura){
  cout << "construtor de sobrecarga da classe quadrado" << endl;
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}

Quadrado::~Quadrado(){
  cout << "destrutor da classe quadrado" << endl;
}
void Quadrado::imprime(){

  std::cout << "area: " << calcula_area() << std::endl;
  std::cout << "perimetro: " << calcula_perimetro() << std::endl;
}
