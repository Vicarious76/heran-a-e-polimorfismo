#include <iostream>
#include "circulo.hpp"
#include "hexagono.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "pentagono.hpp"
#include "paralelograma.hpp"
#include <string>
#include <vector>


int main(){
  Quadrado quadrado0;
  Circulo circulo0;
  Triangulo triangulo0;
  Hexagono hexagono0;
  Pentagono pentagono0;
  Paralelograma paralelograma0;


  quadrado0.set_tipo("QUADRADO");
  quadrado0.set_base(4);
  quadrado0.set_altura(8);
  quadrado0.calcula_area();
  circulo0.calcula_perimetro();
  cout << "QUADRADO\n" << endl;
  quadrado0.imprime();
  cout<<endl;

  circulo0.set_tipo("CIRCULO");
  circulo0.set_raio(2);
  circulo0.set_base(6);
  circulo0.set_altura(12);
  circulo0.calcula_area();
  circulo0.calcula_perimetro();
  cout << "CIRCULO\n" << endl;
  circulo0.imprime();
  cout<<endl;


  triangulo0.set_tipo("TRIANGULO");
  triangulo0.set_base(5);
  triangulo0.set_altura(10);
  triangulo0.calcula_area();
  triangulo0.calcula_perimetro();
  cout << "TRIANGULO\n" << endl;
  triangulo0.imprime();
  cout<<endl;

  hexagono0.set_tipo("HEXAGONO");
  hexagono0.set_base(4);
  hexagono0.set_altura(8);
  hexagono0.calcula_area();
  hexagono0.calcula_perimetro();
  cout << "HEXAGONO\n" << endl;
  hexagono0.imprime();
  cout<<endl;

  pentagono0.set_tipo("PENTAGONO");
  pentagono0.set_base(5);
  pentagono0.set_altura(5);
  pentagono0.calcula_area();
  pentagono0.calcula_perimetro();
  cout << "PENTAGONO\n" << endl;
  pentagono0.imprime();
  cout<<endl;

  paralelograma0.set_tipo("PARALELOGRAMA");
  paralelograma0.set_base(5);
  paralelograma0.set_altura(6);
  paralelograma0.calcula_area();
  paralelograma0.calcula_perimetro();
  cout << "PARALELOGRAMA\n" << endl;
  paralelograma0.imprime();
  cout<<endl;

  return 0;
}
