#include "paralelograma.hpp"
#include <iostream>

Paralelograma::Paralelograma(){
  cout << "construtor da classe Paralelograma" << endl;
}
Paralelograma::Paralelograma(string tipo, float base, float altura){
  cout << "contrutor de sobrecarga da classe Paralelograma" << endl;
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}
Paralelograma::~Paralelograma(){
  cout << "destrutor da classe Paralelograma" << endl;
}
void Paralelograma::imprime(){
  std::cout << "area "<< calcula_area() << std::endl;
  std::cout << "area "<< calcula_perimetro() << std::endl;
}

float Paralelograma::calcula_perimetro(){
  return (2*(get_altura() + get_base()));
}
