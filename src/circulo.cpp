#include "circulo.hpp"
#include <iostream>
#include <math.h>

Circulo::Circulo(){
  cout << "construtor da classe circulo "<< endl;
  raio = 0.0;
}

Circulo::Circulo(string tipo, float base, float altura){
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}



Circulo::~Circulo(){
  cout << "destrutor da classe circulo" << endl;
}

void Circulo::imprime(){

  std::cout << "area: "<< calcula_area() << std::endl;
  std::cout << "perimetro: "<< calcula_perimetro() << std::endl;

}

void Circulo::set_raio(float raio){
  this->raio = raio;
}

float Circulo::get_raio(){
  return raio;
}

float Circulo::calcula_area(){
  return ((get_raio() * get_raio()) * 3.14);
}

float Circulo::calcula_perimetro(){
  return (2 * 3.14 * get_raio());
}
