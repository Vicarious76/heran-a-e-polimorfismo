#include "triangulo.hpp"
#include <iostream>

Triangulo::Triangulo(){
  cout << "construtor da classe triangulo" << endl;
}
Triangulo::Triangulo(string tipo, float base, float altura){
  cout << "contrutor de sobrecarga da classe triangulo" << endl;
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}
Triangulo::~Triangulo(){
  cout << "destrutor da classe triangulo" << endl;
}
void Triangulo::imprime(){
  std::cout << "ar a"<< calcula_area() << std::endl;
  std::cout << "perimetro "<< calcula_perimetro() << std::endl;
}

float Triangulo::calcula_area(){
  return ((get_altura()*get_base())/2);
}

float Triangulo::calcula_perimetro(){
  return (2 * get_altura() + get_base());
}
