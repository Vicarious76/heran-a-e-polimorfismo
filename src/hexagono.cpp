#include "hexagono.hpp"
#include <iostream>
#include <math.h>

Hexagono::Hexagono(){
  cout << "Construtor de classe hexagono" << endl;
}
Hexagono::Hexagono(string tipo, float base, float altura){
  cout << "construtor de sobrecarga da classe hexagono" << endl;
  set_tipo(tipo);
  set_base(base);
  set_altura(altura);
}

Hexagono::~Hexagono(){
  cout << "destrutor da classe hexagono" << endl;
}

void Hexagono::imprime(){
  std::cout << "AREA:"<< calcula_area() << std::endl;
  std::cout << "PERIMETRO:"<< calcula_perimetro() << std::endl;
}

float Hexagono::calcula_area(){
  return ((get_altura() * 3 *sqrt(3))/2 *get_altura() * get_altura());
}

float Hexagono::calcula_perimetro(){
  return (get_altura() * 6);
}
